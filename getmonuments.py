#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from os import path
from pprint import pprint
import requests
from pywiki_light import *

config = configparser.ConfigParser()
config.read(os.path.dirname(__file__) + 'config.ini')

comwiki = Pywiki("commonswiki")
comwiki.login()


COMMONS_ROOT = "https://commons.wikimedia.org"

countries = [
    "Albania",
    "Algeria",
    "Andorra",
    "Antarctica",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bangladesh",
    "Basque Country",
    "Belarus",
    "Belgium  and  Luxembourg",
    "Bolivia",
    "Brazil",
    "Bulgaria",
    "Cameroon",
    "Canada",
    "Catalonia",
    "Chile",
    "China",
    "Colombia",
    "Croatia",
    "Czech Republic",
    "Denmark",
    "Dutch Caribbean",
    "Egypt",
    "El Salvador",
    "Estonia",
    "Finland",
    "France",
    "Georgia",
    "Germany",
    "Ghana",
    "Greece",
    "Hong Kong",
    "Hungary",
    "India",
    "Iran",
    "Iraq",
    "Ireland",
    "Israel",
    "Italy",
    "Jordan",
    "Kenya",
    "Kosovo",
    "Latvia",
    "Lebanon",
    "Malaysia",
    "Malta",
    "Mexico",
    "Morocco",
    "Nepal",
    "Netherlands",
    "Nigeria",
    "North Macedonia",
    "Norway",
    "Pakistan",
    "Palestine",
    "Panama",
    "Peru",
    "Philippines",
    "Poland",
    "Portugal",
    "Pyrenees Mediterranean",
    "Romania",
    "Russia",
    "Saudi Arabia",
    "Serbia",
    "Slovakia",
    "South Africa",
    "South Korea",
    "Spain",
    "Sweden",
    "Switzerland",
    "Syria",
    "Taiwan",
    "Thailand",
    "Tunisia",
    "Uganda",
    "Ukraine",
    "United Kingdom",
    "United States",
    "United States of America",
    "Uruguay",
    "Venezuela",
]


def parse_photos_page(page_name):
    try:
        print("Parsing {} page".format(page_name))
        photos_page = path.join(COMMONS_ROOT, "wiki", page_name)
        page = requests.get(photos_page)
        photos_soup = BeautifulSoup(page.text, "html.parser")

        galleries = photos_soup.find_all("ul", "gallery")
        galleries_files = []
        for g in galleries:
            title = g.find_previous_sibling(["h2", "h3"])

            if title:
                title = title.span.text
            else:
                "Notitle"

            thumbs = g.select(".thumb")
            files = []
            for t in thumbs:
                file_name = t.a["href"][6:]
                files.append(file_name)
            galleries_files.append((title, files))
        return galleries_files

    except Exception:
        raise Exception("Could not parse page {}".format(page_name))


def get_picture_data(files):
    responses = comwiki.request({
        "action": "query",
        "format": "json",
        "prop": "imageinfo",
        "titles": "|".join(files),
        "iiprop": "extmetadata"
    })
    results= {}
    if 'query' in responses:
        for pageid, data in responses["query"]["pages"].items():
            filename = data['title']
            content = {}
            imageinfo = data['imageinfo'][0]['extmetadata']
            pprint(filename)
            pprint(imageinfo)
            content['title'] = imageinfo['ObjectName']['value']
            content['categories'] = imageinfo['Categories']['value']
            content['artist'] = imageinfo['Artist']['value']
            content['license-name'] = imageinfo['LicenseShortName']['value']
            content['license-url'] = imageinfo['LicenseUrl']['value']
            results['filename'] = content


# for y in range(2010, 2018):
for y in [2010]:
    cat = "Wiki_Loves_Monuments_{}_winners".format(y)
    results = parse_photos_page(cat)
    for r in results:
        # In 2010 only the Netherlands participated.
        if y == 2010:
            r = ("Netherlands", r[1])
        pprint(r)
        if r[0] and r[0] in countries:
            country = r[0]
            files = ("|").join(r[1])
            pprint(files)
            filedata = get_picture_data(r[1])
            pprint(filedata)
